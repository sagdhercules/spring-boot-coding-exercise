package com.telstra.codechallenge.helloworld;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class RepoWrapper {
  private long total_count;
  private String incomplete_results;
  private List<Repos> items;
  
}
