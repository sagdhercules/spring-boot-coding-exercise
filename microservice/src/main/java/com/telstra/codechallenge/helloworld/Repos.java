package com.telstra.codechallenge.helloworld;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Repos {
  private String html_url;
  private long watchers_count;
  private String language;
  private String description;
  private String name;  
  
}
