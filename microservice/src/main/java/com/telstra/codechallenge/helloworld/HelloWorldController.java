package com.telstra.codechallenge.helloworld;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;



@RestController
public class HelloWorldController {
	

	@Autowired
	RestTemplate restTemplate;
	
	@Value("${zeroFollowerAccounts.base.url}")
	private String zeroFollowerAccountsUrl;
	
	
	@Value("${hottestRepos.base.url}")
	private String hottestReposUrl;
	
  private static final String TEMPLATE = "Hello, %s!";
  private final AtomicLong counter = new AtomicLong();

  @RequestMapping(path = "/hello", method = RequestMethod.GET)
  public HelloWorld hello(@RequestParam(value = "name", defaultValue = "World") String name) {
    return new HelloWorld(counter.incrementAndGet(), String.format(TEMPLATE, name));
  }
  
  
  @RequestMapping(path = "/zeroFollowerAccounts", method = RequestMethod.GET)
  public List<Accounts> zeroFollowerAccounts(@RequestParam(value = "noOfAccounts", defaultValue = "10") String noOfAccounts) {
	  List<Accounts> accountsList = new ArrayList<Accounts>();
	  ResponseEntity<AccountWrapper> rpe=restTemplate.getForEntity(zeroFollowerAccountsUrl, AccountWrapper.class);
	  AccountWrapper accWrap = rpe.getBody();
	  if(accWrap!=null && accWrap.getItems()!=null && !accWrap.getItems().isEmpty()) {
		  int resultSize = Integer.parseInt(noOfAccounts);
		  if(accWrap.getItems().size()<resultSize) {
			  resultSize = accWrap.getItems().size();
		  }
		  accountsList=accWrap.getItems().stream().limit(resultSize).collect(Collectors.toList());
	  }
	  return accountsList;
  }
  
  
  @RequestMapping(path = "/hottestRepos", method = RequestMethod.GET)
  public List<Repos> hottestRepos(@RequestParam(value = "noOfRepos", defaultValue = "10") String noOfRepos) {
	  List<Repos> reposList = new ArrayList<Repos>();
	  ResponseEntity<RepoWrapper> rpe=restTemplate.getForEntity(hottestReposUrl, RepoWrapper.class);
	  RepoWrapper repoWrap = rpe.getBody();
	  if(repoWrap!=null && repoWrap.getItems()!=null && !repoWrap.getItems().isEmpty()) {
		  int resultSize = Integer.parseInt(noOfRepos);
		  if(repoWrap.getItems().size()<resultSize) {
			  resultSize = repoWrap.getItems().size();
		  }
		  reposList=repoWrap.getItems().stream().limit(resultSize).collect(Collectors.toList());
	  }
	  return reposList;
  }
  
}
