package com.telstra.codechallenge.helloworld;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class Accounts {
  private long id;
  private String login;
  private String html_url;
  
}
