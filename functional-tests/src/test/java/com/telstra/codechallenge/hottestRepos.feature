# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As a developer i want to test the hottestRepos uri

  Scenario: Is the hottestRepos uri available and functioning
    Given url microserviceUrl
    And path '/hottestRepos'
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://api.github.com/search/repositories?q=created:>2020-05-15&sort=stars&order=desc
    And match response == 
    """
    [{      
      html_url : '#string',
      watchers_count : '#number', 
      language : '#string',
      description : '#string',
      name : '#string'
    },
    {      
      html_url : '#string',
      watchers_count : '#number', 
      language : null,
      description : '#string',
      name : '#string'
    },{      
      html_url : '#string',
      watchers_count : '#number', 
      language : '#string',
      description : '#string',
      name : '#string'
    },{      
      html_url : '#string',
      watchers_count : '#number', 
      language : '#string',
      description : '#string',
      name : '#string'
    },{      
      html_url : '#string',
      watchers_count : '#number', 
      language : '#string',
      description : '#string',
      name : '#string'
    }
    ,{      
      html_url : '#string',
      watchers_count : '#number', 
      language : '#string',
      description : '#string',
      name : '#string'
    },{      
      html_url : '#string',
      watchers_count : '#number', 
      language : '#string',
      description : '#string',
      name : '#string'
    },{      
      html_url : '#string',
      watchers_count : '#number', 
      language : '#string',
      description : '#string',
      name : '#string'
    },{      
      html_url : '#string',
      watchers_count : '#number', 
      language : '#string',
      description : '#string',
      name : '#string'
    },{      
      html_url : '#string',
      watchers_count : '#number', 
      language : '#string',
      description : '#string',
      name : '#string'
    }]
    And match response.content == "[
{
html_url: "https://github.com/vincentdoerig/latex-css",
watchers_count: 1091,
language: "HTML",
description: "LaTeX.css is a CSS library that makes your website look like a LaTeX document",
name: "latex-css"
},
{
html_url: "https://github.com/dair-ai/ml-visuals",
watchers_count: 562,
language: null,
description: "Visuals contains figures and templates which you can reuse and customize to improve your scientific writing.",
name: "ml-visuals"
},
{
html_url: "https://github.com/ianzhao05/textshot",
watchers_count: 336,
language: "Python",
description: "Python tool for grabbing text via screenshot",
name: "textshot"
},
{
html_url: "https://github.com/idris-lang/Idris2",
watchers_count: 332,
language: "Idris",
description: "A purely functional programming language with first class types",
name: "Idris2"
},
{
html_url: "https://github.com/LeonidasEsteban/bookmark-landing",
watchers_count: 316,
language: "CSS",
description: "Challenge #3 by FrontendMentor.io",
name: "bookmark-landing"
},
{
html_url: "https://github.com/utkusen/shotlooter",
watchers_count: 315,
language: "Python",
description: "a recon tool that finds sensitive data inside the screenshots uploaded to prnt.sc",
name: "shotlooter"
},
{
html_url: "https://github.com/hitherejoe/ComposeAcademy-Playground",
watchers_count: 298,
language: "Kotlin",
description: "Playground project for the Jetpack Compose APIs",
name: "ComposeAcademy-Playground"
},
{
html_url: "https://github.com/hkgumbs/multi",
watchers_count: 282,
language: "Swift",
description: "Create a custom, lightweight macOS app from a group of websites",
name: "multi"
},
{
html_url: "https://github.com/immuni-app/immuni-app-android",
watchers_count: 269,
language: "Kotlin",
description: " Official repository for the Android version of the immuni application ",
name: "immuni-app-android"
},
{
html_url: "https://github.com/LeonidasEsteban/rock-paper-scissors-react",
watchers_count: 262,
language: "JavaScript",
description: "Challenge #5 from Frontend Mentor",
name: "rock-paper-scissors-react"
}
]"
    """


