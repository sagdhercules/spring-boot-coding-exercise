# See
# https://github.com/intuit/karate#syntax-guide
# for how to write feature scenarios
Feature: As a developer i want to test the zeroFollowerAccounts uri

  Scenario: Is the zeroFollowerAccounts uri available and functioning
    Given url microserviceUrl
    And path '/zeroFollowerAccounts'
    When method GET
    Then status 200
    And match header Content-Type contains 'application/json'
    # see https://api.github.com/search/users?q=followers:0&sort=joined&order=desc
    And match response == 
    """
    [{ id : '#number',
      login : '#string',
      html_url : '#string'
    },{ id : '#number',
      login : '#string',
      html_url : '#string'
    },{ id : '#number',
      login : '#string',
      html_url : '#string'
    },{ id : '#number',
      login : '#string',
      html_url : '#string'
    },{ id : '#number',
      login : '#string',
      html_url : '#string'
    },{ id : '#number',
      login : '#string',
      html_url : '#string'
    },{ id : '#number',
      login : '#string',
      html_url : '#string'
    },{ id : '#number',
      login : '#string',
      html_url : '#string'
    },{ id : '#number',
      login : '#string',
      html_url : '#string'
    },{ id : '#number',
      login : '#string',
      html_url : '#string'
    }]
    And match response.content == "[
{
id: 65907258,
login: "npez89",
html_url: "https://github.com/npez89"
},
{
id: 65907256,
login: "ZJMCJNB",
html_url: "https://github.com/ZJMCJNB"
},
{
id: 65907257,
login: "Pieper1956",
html_url: "https://github.com/Pieper1956"
},
{
id: 65907255,
login: "josefleeb1",
html_url: "https://github.com/josefleeb1"
},
{
id: 65907253,
login: "hao1044204363",
html_url: "https://github.com/hao1044204363"
},
{
id: 65907252,
login: "vbrignatz",
html_url: "https://github.com/vbrignatz"
},
{
id: 65907249,
login: "Saise-g",
html_url: "https://github.com/Saise-g"
},
{
id: 65907250,
login: "DARFORC",
html_url: "https://github.com/DARFORC"
},
{
id: 65907246,
login: "markmark22",
html_url: "https://github.com/markmark22"
},
{
id: 65907248,
login: "foliat",
html_url: "https://github.com/foliat"
}
]"
    """


